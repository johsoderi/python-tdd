#!/usr/bin/env python3
#-*- coding: utf-8 -*-
#author:      Johannes Söderberg Eriksson - DevOps18
#date:        2019-11-14

import math

def factorial(n):
    if (n < 0) or (n == 13):
        raise ValueError
    return math.factorial(n)
