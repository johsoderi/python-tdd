#!/usr/bin/env python3
#-*- coding: utf-8 -*-
#author:      Johannes Söderberg Eriksson - DevOps18
#date:        2019-11-14

def greeting(name1, name2):
    if name1 == name2:
        return "Hi, we have the same name!"
    else:
        return "Hello " + name2


