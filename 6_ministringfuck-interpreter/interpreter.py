#!/usr/bin/env python3
#-*- coding: utf-8 -*-
#author:      Johannes Söderberg Eriksson - DevOps18
#date:        2019-11-14

def evaluate(program):
    mem = 0
    output = ""
    for i in range(len(program)):
        if program[i] == '+':
            mem += 1
            if mem == 256:
                mem = 0
        if program[i] == '.':
            output += chr(mem)
    return output
