#!/usr/bin/env python3
#-*- coding: utf-8 -*-
#description: Assembly language interpreter - 4 instructions
#author:      Johannes Söderberg Eriksson - DevOps18
#date:        2019-11-14
#version:     1.1

def evaluate(prg):
    pntr = 0
    end = len(prg)
    reg = {}
    while (pntr < end):
        line = prg[pntr].split(' ')
        word = line[0]
        ref1 = line[1]
        ref2 = line[2] if line[2:] else None
        if word == "mov":
            reg[ref1] = int(reg[ref2]) if ref2 in reg else int(ref2)
        if word == "inc":
            reg[ref1] = (int(reg[ref1])+1) if ref1 in reg else 1
        if word == "dec":
            reg[ref1] = (int(reg[ref1])-1) if ref1 in reg else -1
        if word == "jmp":
            pntr += int(ref2) if reg.get(ref1) != 0 else 1
        pntr += 1 if word != "jmp" else 0
    return reg
