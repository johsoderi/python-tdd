#!/usr/bin/env python3
#-*- coding: utf-8 -*-
#author:      Johannes Söderberg Eriksson - DevOps18
#date:        2019-11-14

def distinct(year):
    year+=1
    while len(set(str(year))) < len(str(year)):
        year+=1
    return year
