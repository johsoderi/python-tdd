#!/usr/bin/env python3
#-*- coding: utf-8 -*-
#author:      Johannes Söderberg Eriksson - DevOps18
#date:        2019-11-14

def first_non_consecutive(arr):
    for x in range(len(arr)):
        if (x > 0) and ( arr[x] != ( arr[x-1] + 1) ):
            return arr[x]
    return None
