**Non-consecutive integer challenge**

Given an array of sorted integers (from smallest to largest), find the first
integer which is not consecutive.

Look at the test cases for more explaination.