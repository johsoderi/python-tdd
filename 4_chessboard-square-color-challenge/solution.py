#!/usr/bin/env python3
#-*- coding: utf-8 -*-
#author:      Johannes Söderberg Eriksson - DevOps18
#date:        2019-11-14

def get_color(column, row):
    if ( ( row - ord(column) ) % 2 ) == 0:
        return "black"
    else:
        return "white"
